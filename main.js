// BaseClasses
class Rectangle {
    constructor(length, width) {
        this.id = 100;
        this.length = length;
        this.width = width;
    }
    getArea() { return this.length * this.width; }

    toString() { return "[Rectangle " + this.id + ":" + this.length + "x" + this.width + "]"; }
}

class Square extends Rectangle {
    constructor(size) {
        super(size, size);
        this.id = 101;
    }
    toString() { return "[Square " + this.id + ":" + this.length + "x" + this.width + "]"; }
}


// Build object tree
let square = new Square(5);
square.id = 200;
let rectangle = new Rectangle(3, 4);
rectangle.id = 300;

let objects = [square, rectangle];

var cy = cytoscape({
    container: document.getElementById('cy'),
    layout: {
        name: 'cose',
        fit: true,
        padding: 30,
        avoidOverlap: true,
        minDist: 20,
    },
    style: [
        {
            selector: 'node',
            style: {
                'content': 'data(name)',
                'font-size' : '9'
            }
        },

        {
            selector: 'edge',
            style: {
                'curve-style' : 'straight',
                'target-arrow-shape': 'triangle'
            }
        },
    ],
});


visualize(objects);


function visualize(objs) {
    objs.forEach(function (s, i, o) {
        console.log("#####");
        analyse(s);
    });

    let layout = cy.layout({
        name: 'cose',
        fit: true,
        padding: 30,
        avoidOverlap: true,
        minDist: 50,
    });
    layout.run();
}

function analyse(obj, child) {
    var name = obj.constructor.name;
    if (obj.isPrototypeOf(child))
        name += ".prototype";

    if (cy.getElementById(obj.toString()).length === 0 ) {
        cy.add({data: {id: obj.toString(), name: name}});
        console.log(name);

        Object.getOwnPropertyNames(obj).forEach(function (s, i, o) {
            cy.add({data: {id: obj.toString() + s, name: s}});
            cy.add({data: {id: s + obj.toString(), source: obj.toString() + s, target: obj.toString()}});
            console.log(" -- ", s);
        });
    }

    if (child != null){
        cy.add({data: {
            id: child.toString() + '-' + obj.toString() + Math.floor(Math.random() * (9999 - 1000 + 1)) + 1000,
            source: child.toString(),
            target: obj.toString()}});
        console.log("<- prototype of ", child.constructor.name);
    }

    if (Object.getPrototypeOf(obj) != null) analyse(Object.getPrototypeOf(obj), obj);
}
